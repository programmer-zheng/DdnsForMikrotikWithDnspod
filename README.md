# DdnsForMikrotikWithDnspod

#### 介绍
使用Mikrotik（ROS）自带脚本命令`fetch`请求`DnsPod`的`ddns`接口，实现动态域名解析，避免架设服务器、网站等操作


#### 前置条件

1. 拥有自己的域名（并设置域名解析服务商为DnsPod）
2. 添加Dnspod的Token
3. 添加json转换函数 https://github.com/Winand/mikrotik-json-parser

#### 使用说明
``` python
# DDNS Token
:local dnspodtoken "1234,xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
#record name
:local record "abc";
# domain name 
:local domain "baidu.com";

# 请勿随意更改以下代码 (do not change any code below)
# set file name to save dnspod api results
:local fileName "DnsPodRecordInfo.txt"
/file print file=$fileName;
/file set $fileName contents="";


:global current;

# 获取当前拨号的外网IP
:local IpAddress [/ip address get [find interface="pppoe-out1"] address];

# 判断当前IP与之前的IP是否一致
:if ($IpAddress != $current) do={
    # set query record info request data
    :local recordInfoPostData "lang=cn&format=json&login_token=$dnspodtoken&domain=$domain&sub_domain=$record";
    /tool fetch http-method=post dst-path="$fileName" http-content-type="application/x-www-form-urlencoded" url="https://dnsapi.cn/Record.List" http-data="$recordInfoPostData";

    #load JParseFunctions to parse json string 
    /system script run "JParseFunctions"; global JSONLoad; global JSONLoads; global JSONUnload;
    local content [/file get $fileName contents];

    # dnspod record id 
    local recordId ([$JSONLoads $content]->"records"->0->"id");


    # 截取真实IP，去掉子网掩码
    :local ip [:pick $IpAddress 0 [:find $IpAddress "/"]]
    :local before [:pick $current 0 [:find $current "/"]]

    # 设置nat转换，端口映射地址
    /ip fir nat set [find chain=dstnat] dst-address=$ip

    # 拼接请求参数
    :local postdata "format=json&login_token=$dnspodtoken&domain=$d&record_id=$recordid&sub_domain=$r&record_line_id=0&value=$ip";
    /tool fetch http-method=post keep-result=no http-content-type="application/x-www-form-urlencoded" url="https://dnsapi.cn/Record.Ddns" http-data="$postdata" 
    :set current $IpAddress;

    # unload JParseFunctions
    $JSONUnload

    :log info "ip address $current changed to $IpAddress";
}
```

